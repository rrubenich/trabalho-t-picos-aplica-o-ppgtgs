angular.module('PPGTGS.controllers', ['PPGTGS.services'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

})

.controller('HomeCtrl', function($scope) {
  $scope.title = "PPGTGS",

  $scope.text = 'PROGRAMA DE PÓS-GRADUAÇÃO STRICTO SENSU EM TECNOLOGIAS, GESTÃO E SUSTENTABILIDADE NÍVEL DE MESTRADO PROFISSIONAL';
  
  $scope.coords = [
    { name: 'Prof. Carlos Henrique Zanelato Pantaleão', status: 'Coordenador Especial'},
    { name: 'Elizete Aparecida Zanellatto Pimenta', status: 'Assistente'}
  ];

  $scope.info = {
    fone: '(45) 4334-0904',
    location: 'Parque Tecnologico Itaipu',
    location_sub: 'Bloco 06 - Espaço 2',
    adrress: 'Av. Tancredo Neves, 6731',
    cep: '85856-970',
    city: 'Foz do Iguaçu - Paraná'
  }
})


.controller('TeachersCtrl', function($scope, Teachers, teachers) {
  $scope.title = "Professores",
  $scope.teachers = teachers;
})

.controller('TeacherCtrl', function($scope, $stateParams, Teachers) {
  $scope.teacher = Teachers.get($stateParams.teacherId);
})

.controller('RegulationsCtrl', function($scope, Regulations, regulations) {
  $scope.title = "Regulamentos e Normas",
  $scope.regulations = regulations;
})

.controller('NoticesCtrl', function($scope, Notices, notices) {
  $scope.title = "Editais",
  $scope.notices = notices;
})


;
