angular.module('PPGTGS', ['ionic', 'PPGTGS.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider


  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.concentration_area', {
    url: '/concentration_area',
    views: {
      'menuContent': {
        templateUrl: 'templates/concentration_area.html'
      }
    }
  })

  .state('app.research_line', {
    url: '/research_line',
    views: {
      'menuContent': {
        templateUrl: 'templates/research_line.html'
      }
    }
  })

  .state('app.teachers', {
    url: '/teachers',
    views: {
      'menuContent': {
        templateUrl: 'templates/teachers.html',
        controller: 'TeachersCtrl',
        resolve: {
          teachers: function(Teachers) {
            return Teachers.all(); }
        }
      }
    }
  })

  .state('app.teacher', {
    url: '/teachers/:teacherId',
    views: {
      'menuContent': {
        templateUrl: 'templates/teacher.html',
        controller: 'TeacherCtrl'
      }
    }
  })

  .state('app.themes', {
    url: '/themes',
    views: {
      'menuContent': {
        templateUrl: 'templates/themes.html'
      }
    }
  })

  .state('app.process', {
    url: '/process',
    views: {
      'menuContent': {
        templateUrl: 'templates/process.html'
      }
    }
  })

  .state('app.calendar', {
    url: '/calendar',
    views: {
      'menuContent': {
        templateUrl: 'templates/calendar.html'
      }
    }
  })

  .state('app.regulation', {
    url: '/regulation',
    views: {
      'menuContent': {
        templateUrl: 'templates/regulation.html',
        controller: 'RegulationsCtrl',
        resolve: {
          regulations: function(Regulations) {
            return Regulations.all(); }
        }
      }
    }
  })

  .state('app.notices', {
    url: '/notices',
    views: {
      'menuContent': {
        templateUrl: 'templates/notices.html',
        controller: 'NoticesCtrl',
        resolve: {
          notices: function(Notices) {
            return Notices.all(); }
        }
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
