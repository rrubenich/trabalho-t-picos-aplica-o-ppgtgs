angular.module('PPGTGS.services', [])

.factory('Teachers', function ($http, $q) {
  var teachers = [];
  return {
    all: function () {
      var dfd = $q.defer();
      $http.get("/data/teachers.json").then(function(response){
        teachers = response.data;
        dfd.resolve(teachers);
      });
      return dfd.promise;
    },

    get: function(teacherId) {
       for (var i = 0; i < teachers.length; i++) {
        if (teachers[i].id === parseInt(teacherId)) {

          return teachers[i];
        }
      }
      return null;
    }
  };
})


.factory('Notices', function ($http, $q) {
  var notices = [];
  return {
    all: function () {
      var dfd = $q.defer();
      $http.get("/data/notices.json").then(function(response){
        notices = response.data;
        dfd.resolve(notices);
      });
      return dfd.promise;
    }
  };
})

.factory('Regulations', function ($http, $q) {
  var regulations = [];
  return {
    all: function () {
      var dfd = $q.defer();
      $http.get("/data/regulations.json").then(function(response){
        regulations = response.data;
        dfd.resolve(regulations);
      });
      return dfd.promise;
    }
  };
});